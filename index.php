<?php
require ("Animal.php");
require ("Ape.php");
require ("Frog.php");

$sheep = new Animal("Shaun");
echo "Nama Hewan : $sheep->name <br>"; 
echo "Legs :  $sheep->legs <br>";
echo "Cold Blooded :  $sheep->cold_blooded <br><br>";

$sungkong = new Ape("Kera Sakti");
echo "Nama Hewan : $sungkong->name <br>"; 
echo "Legs :  $sungkong->legs <br>";
echo "Cold Blooded :  $sungkong->cold_blooded <br>";
echo $sungkong->yell();
echo "<br><br>";

$kodok = new Frog("Buduk");
echo "Nama Hewan : $kodok->name <br>"; 
echo "Legs :  $kodok->legs <br>";
echo "Cold Blooded :  $kodok->cold_blooded <br>";
echo $kodok->jump();

?>